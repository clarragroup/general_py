
import numpy as np
from plancknu import plancknu

def radtran_lorestrans5fast(wnm, od, transL, tzl, tave):
    """
    def radtran_lorestrans5fast(wnm, od, transL, tzl, tave):
    
    by Penny Rowe
    May 30, 2001
    Updated Aug. 7, 2002
    Updated Feb. 2012: added odDir
    Updated May 23, 2013: 
    	Improved comments
    	Changed rdODsingle to rdODprec, added "prec" (precision) to inputs
    	Changed name from radtranC to radtran_lorestrans
    	Added possibilities "lowClough13" and "lowCloughPade" to bltype
    	No longer cd to directory where ods are
    Updated Jan. 11, 2013
    	Can do up or downwnwelling radiance, based on direction
    
    Purpose: 
    	Get low- or perfect-resolution radiance (and corresponding wavenumber)
    	by performing radiative transfer in the same way as LBLRTM,
    	except that Eqn (13) of Clough et al. 1992 is used instead of
    	getting the Planck function empirically using a Pade coefficient
    	as in Eqn (15), *or* using the Pade function, or using the method
    	of Wiscombe 1976.  Also, can do low or perfect resolution.  These
    	Choice depend on input variable bltype.
    
    	Low-resolution is acheived by reducing the resolution of the
    	transmittances, not the radiances
    
    .. Inputs to function call:
    	 wnm  = wavenumber, numpy 1-d array
    	 od   = layer optical depths, numpy array [itop x len(wnm)]
    	 itop = number of layers for which there are ods
    	 tzl  = temperature at layer bottom, from surface up
    	 tave = "ave" temperature, as defined by LBLRTM
    	 viewangle = viewing angle, in degrees
    
    	 Old, obsolete:
    	 dnu  = desired resolution
    	 bltype: lowClough13, lowClough/lowCloughPade, hiClough13, lowWiscombe
    
    	 izeroout  removed!
    
    .. Outputs:
    	 rad: radiance, numpy 1-d array
    
    .. Updates:
    	 Accounting for off-zenith viewing angle. Done by dividing
    	 the optical depth by cosine(angle_in_radians) throughout.
    """
    
    itop = len(tzl)-1
    if od.shape != (len(wnm),itop):
    	raise ValueError('Input dimensions of optical depth are wrong.')
    
    # tave is not quite the average layer temperature. If it is not
    # given, calculate it as the average temperature anyway    
    if tave==0:
    	tave = (tzl[:itop]+tzl[1:itop+1])/2 
    	#fprintf('\n tave=0, so creating tave from temps.')
    
    # Downwelling radiance.  Start from surface and go up
    ilayers = list(range(itop))
    
    nulen  = len(wnm)
    rad    = np.zeros((nulen, 1))
    Rbelow = np.zeros((nulen, itop+1))
    Bctsc  = np.zeros((nulen, itop+1))
    # get transmittance, rad., from the bottom layer up
    for ilayer in ilayers:
    		
    	# Uses low resolution layer optical depth
    	# See Clough et al. 1992 Eqn. (13)
    	# PMR May 23, 2013
    	Bb   =  plancknu(wnm, tave[ilayer])
    	Bu   =  plancknu(wnm, tzl[ilayer])
    			
    	if ilayer==1:            # Should be 0?
    		Bctsc[:,0] = Bu
    
    	atau = 0.278 * od[:,ilayer]                              # Pade coeff. x tau
    	BL = (Bb + atau*Bu)* (1+atau)**-1                        # Eqn (15)
    	rad0 = -BL * (transL[:,ilayer+1]-transL[:,ilayer]) ;     # Eqn (14)
    	if np.any(np.isnan(rad0)):
    		raise ValueError('found an nan')  # end
    
    	Bctsc[:,ilayer+1]  = BL*transL[:,ilayer+1]
    	rad[:,0]  = rad[:,0] + rad0
    	Rbelow[:,ilayer+1] = rad[:,0]
    
    
    # QC
    if sum(np.imag(rad))<1e-10:
    	rad=np.real(rad) 
    else:
    	raise ValueError('Non-trivial imaginary part')
    
    if sum(sum(np.imag(Rbelow)))<1e-10 :
    	Rbelow=np.real(Rbelow) 
    else:
    	raise ValueError('Non-trivial imaginary part')
    
    
    rad     = 1e3 * rad
    Rbelow  = 1e3 * Rbelow
    Bctsc   = 1e3 * Bctsc
    
    return rad, Rbelow, Bctsc
