
from reduce_resolution import reduce_resolution as reduce_res
import numpy as np
from read_od_lblrtm import read_od_lblrtm
from plancknu import plancknu
from os.path import isfile
# import time
# from scipy import interpolate

def radtran_below_cld(od_dir, nlyr_trop, nlyr_od, tzl, tave,
  dnu, bwn, ewn, N, n, zs, cos_angle, prec = 's'):
    """
    Copyright Nov. 30, 2014 by Dr. Penny M. Rowe and NorthWest Research
      Associates. All rights reserved.
    
    Purpose:
       Read in LBLRTM optical depths and get the 
    
    Important note on layering:  
       The variables rads, Bc_tsc, and tsc go from the surface to the
       boundary layer heights. This means that the 0th values are
       from the surface to the surface, or 0 for rads and 1 for tsc. 
       This is a waste of storage space, but saves time later when
       considering a cloud at the surface for co2 slicing. After timings
       are tested, this could be modified. Because of this extra value,
       there are nlyr_trop+1 values in rads, Bc_tsc, and tsc 
       
       For od this is not the case; odlyr is defined to be for the layers,
       not the boundaries, and starts with layer 1, which is for boundaries
       1 through 2.
    
    Inputs in function call:
      od_dir:    directory with LBLRTM optical depths
      nlyr_trop: number of layers up to troposphere
      nlyr_od:   number of layers desired for optical depths and transmittances
      tzl:       temperature at layer bottom
      tave:      "ave" temperature, as defined by LBLRTM
      dnu:       wavenumber spacing (determined by resolution)
      bwn:       beginning wavenumber
      ewn:       ending wavenumber
      cos_angle: cosine of viewing angle, defined as 0 = view straight up
      prec:      precision for   read_od_lblrtm
      
    Outputs (all at instrument resolution unless specified):
      nu:               Final wavenumber
      rads:             Surface-to-layer radiance (0=> surface-to-surface)
      Bc_tsc:           Planck function of T times transmittance below
      tsc:              Surface-to-layer transmittance (0=> surface-to-surface)
      od:               Layer optical depth (0=> layer 1)
      nu_mono:          Monochromatic wavenumber, from lblrtm
      transL_mono_trop: Monochromatic trans from surface to troposphere
    
    Othere Variables used in this code:
         od_mono: monochromatic layer optical depth, from lblrtm
         nu_L: layer wavenumber, at instrument resolution
         odtot: surface-to-layer optical depth, at instrument resolution
         trans: surface-to-layer transmittance, at instrument resolution
         transLm1: above for surface-to-layer-minus-1
         
    """
  
    # ... QC
    if dnu<=0:
        raise NameError('Laser effective wavenumber must be > 0, \
                         this is low resolution.')
  
  
    if od_dir[-1] != '/':
        od_dir += '/'
    
    # .. Inputs for doing radiative transfer
    AA = 0.278                 # Pade coefficient
  
    # .. Sometimes the optical depth may be less than 0 because the
    #    transmittance is greater than one, due to ringing from nearby nus
    #    If the layer optical depth is less than 1e-5, DISORT will zero out
    #    all layers above. So only do this if above ~20 km, otherwise
    #    set to a small but non-zero value.
    minod_below20km = 1.001e-5 #1.001e-3 
    minod_above20km = 1e-6 
    maxod = 100                  # exp(-11)= 1.67e-5
    nuLm1_mono = 0
    #transL_mono_trop = 0
    
    if (type(tave)) == int or (len(tave) == 1):
        tave = (tzl[:len(tzl)-1] + tzl[1:len(tzl)]) / 2
    #elif tave[0] == 0:
    #    tave = (tzl[:len(tzl)-1] + tzl[1:len(tzl)]) / 2
  
    # get transmittance, rad., from the bottom layer up
    nlyr = np.max([nlyr_trop, nlyr_od])

  
    # .. Viewing angle can be downwelling (0) or upwelling (180)
    if cos_angle >= 0:
      # View from surface, so surface up
      ilayers = list(range(1, nlyr+1))
      upflag = 0
    elif cos_angle < 0:
      # View from above, so flip everything
      ilayers = range(nlyr, 1, -1)
      upflag = 1
      raise NameError('Write code for upwelling, based on radtran_belowCld.m')
    else:
      raise NameError('Bad value for cosine of viewing angle')
      
    # .. Before we do all this work, make sure the final optical depth
    #    file exists
    fileinp = 'ODdeflt_' + "%03d" % (ilayers[-1])
    if isfile(od_dir + fileinp) != True:
        raise NameError('Final OD file: ' + fileinp +' does not exist!')

    # .. Get transmittance, rad., from the bottom layer up
    firstlayer = 1; transLm1_mono = 1
    for ilayer in ilayers:
        fileinp = 'ODdeflt_' + "%03d" % (ilayer)
        
        # .. Number of points in long interferogram
        #    z > 50: 2**23  
        #    19 < z < 50:  2**22  
        #    11 < z < 19:  2**21  
        #    0 < z < 11:  2**20  
        if zs[ilayer] > 50:
            n_long = 2**23
        elif zs[ilayer] > 19:
            n_long = 2**22
        elif zs[ilayer] > 11:
            n_long = 2**21
        elif zs[ilayer] > 0:
            n_long = 2**20
        
        
        # .. Load LBLRTM file
        nu_mono, od_mono = read_od_lblrtm(od_dir + fileinp, prec)


        if (not firstlayer) and (len(nu_mono) != len(nuLm1_mono)):
            # interp old stuff up
            transLm1_mono = np.interp(nu_mono, nuLm1_mono, transLm1_mono)
            #spl = interpolate.splrep(nuLm1_mono, transLm1_mono)
            #transLm1_mono = interpolate.splev(nu_mono, spl)
      
            if any(np.isnan(transLm1_mono)):
                raise NameError("NaNs found in transLm1_mono")
    
    
    
        # .. The transmittance up to the bottom of this layer (transLm1_mono)
        #    times the trans of this layer, at monochromatic resolution
        transL_mono = transLm1_mono * np.exp(-od_mono / cos_angle)
    
    
    
        # .. Reduce resolution of transmittance for viewing angle
        nuL, transL = reduce_res(nu_mono, transL_mono, dnu, N, n_long)
    
        # .. Chop to bwn:ewn.  PMR, 2016/08/31
        transL = transL[np.logical_and(nuL>=bwn, nuL<=ewn)]
        nuL = nuL[np.logical_and(nuL>=bwn, nuL<=ewn)]
    
    
        # .. If this is the first layer, initialize some things
        if firstlayer:
            # .. Initialize some array sizes.  Also,
            #    Since a cloud base could be at the surface, the radiance
            #    below the lowest layer (surface) cloud is just zero.
            #    The transmittance is one. The Planck radiance depends on
            #    the surface temperature.
            # [nuL,Bc_tsc0] = aeri(nu_mono,plancknu(nu_mono,tzl(1)),laserNu);
            nu = nuL; nu_len = len(nu)
            rads = np.zeros((nu_len, nlyr_trop+1)) + 0j
            Bc_tsc = np.zeros((nu_len, nlyr_trop+1)) + 0j
            tsc = np.ones((nu_len, nlyr_trop+1)) + 0j
            Bc_tsc[:,0] = plancknu(nu, tzl[0]) + 0j
            od = np.zeros([nu_len, nlyr_od]) + 0j
            odtotLm1 = od[:,0]
       
            # Done with first layer
            firstlayer = 0

        # ... The low resolution wavenumber vector can lose or gain a
        #     single point at the end due to minor differences. If that
        #     happens, we need to interpolate to the standard grid
        if len(nuL) != nu_len or np.sum(nuL - nu)>0 :
            transL = np.interp(nu, nuL, transL).T 

        odtotL = -np.log(transL)
    
        # Do not allow optical depths of transmittances <0 or >exp(-minod)
        odtotL[transL<0] = maxod   # Should this be <= 0?
        if len(zs)==1 and np.isnan(zs[0]):
            odtotL[transL>np.exp(-minod_below20km)] = minod_below20km
        elif zs[ilayer] <= 20:
            odtotL[transL>np.exp(-minod_below20km)] = minod_below20km
        elif zs[ilayer] > 20:
            odtotL[transL>np.exp(-minod_above20km)] = minod_above20km
        
    
        # ... Get Planck function of the layer in the same way as LBLRTM.
        #     BBA corresponds to the lower boundary temperature;  
        #     since the boundary indices start from zero while layers
        #     start from 1, the lower bound is ilayer - 1
        #     Similarly, BB corresponds to the average temperature, which
        #     is for index ilayer-1 for layer ilayer
        XX = AA * od_mono / cos_angle
        BB = plancknu(nu_mono, tave[ilayer-1])
            
        if upflag:
            # Note that tzl is from the bottom up here, because
            # we are going from the TOA down, and
            # tzl(itop) or the temp at TOA, is used first
            BBA = plancknu(nu_mono, tzl[ilayer-1])
        else:
            BBA = plancknu(nu_mono, tzl[ilayer-1])

        BL = (  (BB + XX*BBA) / (1 + XX) )
                

        if (ilayer <= nlyr_trop) or upflag:
            # (Why transmittance to layer top, temperature from base)
            Bc_tsc0 = transL * (plancknu(nuL, tzl[ilayer-1])).T
       
       
            # ... The layer radiance that reaches the surface
            rad0 = -BL * (transL_mono - transLm1_mono) 
            # ... Reduce resolution
            nuL_0, radL = reduce_res(nu_mono, rad0, dnu, N, n_long)
            radL_0 = np.interp(nu, nuL_0, np.real(radL)) #+ \
                     #np.interp(nu, nuL_0, np.imag(radL))*1j
            radL = radL_0.T
       
            # ... Set the radiance-below-cloud and black body cloud emission
            #     for this layer (i.e. the layer with top ibnd2)
            Bctscp = (np.interp(nu, nuL, np.real(Bc_tsc0))).T # + \
                     # (np.interp(nu, nuL, np.imag(Bc_tsc0))).T*1j 
            Bc_tsc[:,ilayer] = Bctscp
            tsc[:,ilayer] = transL  # trans to layer base
            rads[:,ilayer] = radL + rads[:,ilayer-1]   
            
            if ilayer == nlyr_trop:
                transL_mono_trop = transL_mono
    
    
        if (ilayer < nlyr_od) or upflag:
            # Optical depths
            od[:,ilayer-1] = odtotL - odtotLm1 
      
            # Do not allow od<=0
            if len(zs)==1 and np.isnan(zs[0]):
                od[od[:,ilayer-1]<minod_below20km,ilayer-1] = minod_below20km
            elif zs[ilayer-1]<=20:
                od[od[:,ilayer-1]<minod_below20km,ilayer-1] = minod_below20km
            elif zs[ilayer-1]>20:
                od[od[:,ilayer-1]<minod_above20km,ilayer-1] = minod_above20km

      
        # ... Set values for next loop through
        nuLm1_mono = nu_mono
        transLm1_mono = transL_mono
        odtotLm1 = odtotL

  
    # .. Check that the imaginary parts are not too big here
    if np.max(np.imag(tsc)) > 1e-6:
        raise NameError('Imaginary part of transmittance is > 1e-6')
    if np.max(np.imag(od)) > 1e-6:
        raise NameError('Imaginary part of optical depth is > 1e-6')

    rads = 1e3 * np.real(rads)
    Bc_tsc = 1e3 * np.real(Bc_tsc)
    tsc = np.real(tsc)
    od = np.real(od)
  
    return(nu, rads, Bc_tsc, tsc, od, nu_mono, transL_mono_trop)
  

