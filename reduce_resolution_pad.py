# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 19:55:13 2015

@author: prowe
"""


import numpy as np
from ft_stuff import ft
from ft_stuff import ift
# import time
#from scipy import interpolate


def reduce_resolution_pad(nu_mono, calc_mono, dnu, 
                          n_interferogram, n_padded, 
                          n_hi_res):

  """
  Purpose: 
      Reduce resolution according to dnu, assuming n_interferogram pts 
      in interfogram and using n_hi_res points to zero-pad the spectrum
  
  By Penny M. Rowe, 2020/02/21, based on aeri.m
  
  Inputs:
      nu_mono
      calc_mono
      dnu: desired resolution
      n_interferogram: number of points in interferogram
      n_padded: number of points after padding
      n_hi_res: large number of points, for interpolation
       
  
  Notes:
      We want to put the calculated interferogram on the same spacing
      as the measured interferogram so we can chop it at the same
      place. This is defined by NptsInLoResInt
  
      xmax = n_interferogram/laserWn 
      dnu = 1/2/xmax
      numax = dnu*(n_interferogram-1)
      nu = (0:dnu:numax)
  
      Since we are given dnu, rather than laserWn, we have
      xmax = 1/2/dnu
      

      The final wavenumber vector needs to be:
      xmax = NptsInPaddedInt/laserWn ;
      dnu = 1/2/xmax;
      numax = dnu*(NptsInPaddedInt-1);
      wnf=(0:dnu:numax);
  
  """
 
  
  # .. The calculated spectrum needs to begin at zero wavenumbers
  #    (it typically has a spacing of 0.0003 cm-1)
  #    and it needs to end at numax to give dx properly
  bwn = nu_mono[0]
  ewn = nu_mono[-1]
  
  if n_hi_res==0 or np.isnan(n_hi_res):
      n_hi_res = 2**22                     # large number of points


  numax = dnu * n_interferogram            # dnu = lnu/N/2
                                           # so lnu = dnu*N*2
                                           # and numax=lnu/2 or dnu*N
  
  
  nu_mono_interp = np.linspace(0, numax, n_hi_res+1) # e.g. 0.030s
  nu_mono_interp = nu_mono_interp.T
  
  # .. Create new radiance vector with tapered ends. 
  #    Create np.zeros array (0.019 s)
  calc_mono_interp = np.zeros(n_hi_res + 1)
  ind = np.where(np.logical_and(nu_mono_interp >= bwn, nu_mono_interp <= ewn))[0]
  # i1 = int(ind[1])

  
  # Interpolate onto the new grid (0.014 s)
  # this should probably use the cubic interpolation, but there
  # is not a good one in Python and it would take even longer.
  # What are the resulting errors?
  # linear interpolation: 0.013 s for first layer
  # cubic spline: 0.07 to 0.12 s for first - 37th layer
  # Cubic spline adds about 5s
  # However, the improvement is not noticeable.
  calc_mono_interp[ind] = np.interp(nu_mono_interp[ind], nu_mono, calc_mono)
  #spl = interpolate.splrep(nu_mono, calc_mono)
  #calc_mono_interp[ind] = interpolate.splev(nu_mono_interp[ind], spl)
  #print('time to interpolate onto new grid: ' + str(time.time() - start))
	

  #start = time.time()
  # Low-wavenumber roll-off
  # set up index vectors (0.012 s)
  ind_lwn = np.arange(ind[1]); 
  #ind_lwn = np.arange(1e5); 
  #ind_hwn = np.arange(ind[-1], n_hi_res+1); 
  ind_hwn = np.arange(ind[-1], ind[-1]+3000); 
  

  # Compute and paste on the low-wavenumber roll-offs (0.13 s)
  hwn = ind_hwn - ind[-1] + 1
  
  # ind_lwn
  # i1-100000:i1
  calc_mono_interp[ind_lwn] = ( ( np.cos( ind_lwn*np.pi/ind[0] ) )[::-1] +1 ) \
                                  / 2 * calc_mono_interp[ind[0]]
  calc_mono_interp[ind_hwn] = (   np.cos( hwn * np.pi/(len(hwn)   ))  +1 ) \
                                  / 2 * calc_mono_interp[ind_hwn[0]]


  # .. Calculate the interferogram
  #    slow step (e.g. 1.1 s)
  x, ifg = ift(nu_mono_interp, calc_mono_interp, 1, 1)

  
  # .. Pad with zeros starting from the interferogram length+1 + 1
  #    In Python we count from zero, so this is index n_interferogram+1
  #    To the padded length (n_padded+1)
  if (not np.isnan(n_padded)) & (n_padded > n_interferogram):
      ifg[n_interferogram+1:n_padded+1] = 0
  
  # .. Truncate the interferogram to the padded length (n_padded+1)
  #    and take the Fourier Transform to get back the spectrum
  #    Note that in Python we count from zero but also omit the last value
  #    hence 0:n_interferogram+1 => 1st value to n_interferogram+1+1-1th value
  #    or 1st value to n_interferogram+1th value
  #    (0.002 s)
  nu, calc = ft(x[:n_padded+1], ifg[:n_padded+1], 1, 1)
  
  
  #
  # .. Extract the portion of the spectrum between the original
  #    wavenumber limits. (0.0002 s)
  ind = np.where(np.logical_and(nu >= bwn, nu <= ewn))[0]
  nu = nu[ind]
  calc = calc[ind]


  return(nu, calc)


