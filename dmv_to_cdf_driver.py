# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 10:24:46 2018

@author: prowe
"""

from readDMV import readDMV
import matplotlib.pyplot as plt

dirname = '/Volumes/Chile1/Data/SouthPole/Paeri/raw/sparcle_200101/'
filename = dirname + 'ae010101/010101B1.CXS'

# Read a DMV file.
# c1 = readDMV('/Volumes/Chile1/Data/SouthPole/Paeri/raw/sparcle_200101/ae010101/010101C1.RNC')
# f1 = readDMV('/Volumes/Chile1/Data/SouthPole/Paeri/raw/sparcle_200101/ae010101/010101F1.CXS')
# c1 = readDMV('/Volumes/Chile1/Data/SouthPole/Paeri/raw/sparcle_200101/ae010104/010104C1.RNC')

cxs = readDMV(filename)

#plt.plot(c1.wnum1, c1.mean_rad.T)

