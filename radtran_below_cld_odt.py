
from reduce_resolution import reduce_resolution as reduceRes
import numpy as np
from read_od_lblrtm import read_od_lblrtm as rdODprec
from plancknu import plancknu
#import time

def radtran_below_cld_odt(odDir, itop_rad, itop_od, tzl, tave,
  dnu, bwn, ewn, N, n, zs, prec = 's'):
    """
    # by Penny Rowe
    #
    # Nov. 30, 2014
	#
	#
	# Purpose:
	#   Read in LBLRTM optical depths and ...
	#   1) Get the radiance below each height at low resolution
	#      Note that these are layer radiance times transmittance below.
	#   2) Get low resolution transmittances at (up to) each height
	#
	# Important!  Note that rads, Bctsc, and tsc go from the surface to
	#   the boundary layer heights. This means that the first values are
	#   from the surface to the surface, or 0 for rads and 1 for tsc.
	#   For odlayer this is not the case; odlyr is defined to be for the layers, 
    #   not the boundaries, and starts with layer 1, which is for boundaries
    #   1 through 2.
    #
    # Inputs in function call:
    # itop:  number of layers for which there are ods
    # tzl:   temperature at layer bottom
    # tave:    "ave" temperature, as dfined by LBLRTM
    # odDir:   directory with LBLRTM optical depths
    # dnu:     wavenumber spacing (determined by resolution)
    # bwn:
    # ewn:
    # prec:    precision for rdODprec
    # zin:
    # zout:
    #
    # Function calls:
    #   rdODprec: read in LBLRTM optical depths
    #
    # # # # # # # # # # # #  # # # # #  # # # #  # # # #  # # # # # # #
    """
	
    # ... QC
    if dnu<=0:
    	raise NameError('Laser effective wavenumber must be > 0, \
        this is low resolution.')
	
	
    if odDir[-1] != '/':
    	odDir += '/'
    
    # .. Inputs for doing radiative transfer
    AA   = 0.278                 # Pade coefficient
	
    # .. Sometimes the optical depth may be less than 0 because the
    #    transmittance is greater than one, due to ringing from nearby nus
    #    If the layer optical depth is less than 1e-5, DISORT will zero out
    #    all layers above. So only do this if above ~20 km, otherwise
    #    set to a small but non-zero value.
    minod_below20km = 1.001e-5 #1.001e-3 
    minod_above20km = 1e-6 
    maxod = 100                  # exp(-11)= 1.67e-5
    wnm   = 0
    
	
    if len(tave) < len(tzl):
    	tave = (tzl[:len(tzl)-1] + tzl[1:len(tzl)]) / 2
	
	
    # get transmittance, rad., from the bottom layer up
    itop = np.max([itop_rad-1,itop_od])

	
    # get transmittance, rad., from the bottom layer up
    firstlayer=1; transLm1=1; odLm1 = 0
    for ilayer in range(itop):
        if ilayer+1<=9:
            fileinp = 'ODdeflt_00' + str(ilayer+1)
        elif ilayer+1<=99:
            fileinp = 'ODdeflt_0' + str(ilayer+1)
        else:
            fileinp = 'ODdeflt_' + str(ilayer+1)
       
       
        #start = time.time()
        vLp, odp = rdODprec(odDir+fileinp, prec)
        #end = time.time()
        #print('time to load file '+str(ilayer)+' : '+str(end - start))
        
        if (not firstlayer) and (len(vLp) != len(wnm)):
            # interp old stuff up
            #transLm1 = interp1(wnm,transLm1,vLp,'cubic');
            transLm1 = np.interp(vLp, wnm, transLm1)
            odLm1 = np.interp(vLp, wnm, odLm1)
            
            if any(np.isnan(transLm1)):
                raise NameError("NaNs found in transLm1")
    
        tralyr = np.exp(-odp) 
    
    
    	   # ... The transmittance up to the bottom of this layer 
    	   #     (transLm1) times the transmittance of this layer:
        transL = transLm1 * tralyr
     
        # ... The optical depth up to the bottom of this layer
        #     (odLm1) + the optical depth of this layer
        odL = odLm1 + odp


        # ... Reduce resolution
        wnmres0, transL_lores = reduceRes(vLp, transL, dnu, N, n)
        _, odt_lores = reduceRes(vLp, transL*odL, dnu, N, n)
    
        # .. Chop to bwn:ewn.  PMR, 2016/08/31
        transL_lores = transL_lores[np.logical_and(wnmres0>=bwn,wnmres0<=ewn)]
        wnmres0 = wnmres0[np.logical_and(wnmres0>=bwn,wnmres0<=ewn)]
    
    
    	   # ... If this is the first layer, initialize some things
        if firstlayer:
            # ... Initialize some array sizes.  Also,
            #     Since a cloud base could be at the surface, the radiance
            #     below the lowest layer (surface) cloud is just zero.
            #     The transmittance is one. The Planck radiance depends on
            #     the surface temperature.
            #[wnmres0,Bctsc0] = aeri(vLp,plancknu(vLp,tzl(1)),laserNu);
            wnmf  = wnmres0; wnmfLen = len(wnmf)
            rads  = np.zeros((wnmfLen,itop_rad+1)) + 0j
            Bctsc = np.zeros((wnmfLen,itop_rad+1)) + 0j
            tsc   = np.ones((wnmfLen,itop_rad+1)) +0j
            Bctsc[:,0] = plancknu(wnmf,tzl[0]) + 0j
            odlayer = np.zeros([wnmfLen,itop_od]) + 0j
            odtot_below = odlayer[:,0]
            odt = np.zeros((wnmfLen, itop_rad+1)) + 0j
            
            # Done with first layer
            firstlayer = 0

		
        if len(wnmres0) != wnmfLen and np.sum(wnmres0-wnmf)>0 :
            transL_lores = np.interp(wnmf, wnmres0, transL_lores).T 

    
        odtot_lores = -np.log(transL_lores)
    
    	   # Do not allow optical depths of transmittances <0 or >exp(-minod)
        odtot_lores[transL_lores<0] = maxod
        if len(zs)==1 and np.isnan(zs[0]):
            odtot_lores[transL_lores>np.exp(-minod_below20km)] = minod_below20km
        elif zs[ilayer] <= 20:
            odtot_lores[transL_lores>np.exp(-minod_below20km)] = minod_below20km
        elif zs[ilayer] > 20:
            odtot_lores[transL_lores>np.exp(-minod_above20km)] = minod_above20km
      
      
        # ... Get Planck function of the layer in the same way as LBLRTM.
        #     BBA corresponds to the lower boundary temperature;  since
        #     ibnd2 corresponds to layer tops, the lower bound is ibnd2-1
        #     Similarly, BB corresponds to the average temperature, which
        #     is for index ibnd2-1 for layer top ibnd2
        XX     =  AA * odp
        BB     =  plancknu(vLp, tave[ilayer])
        BBA    =  plancknu(vLp, tzl[ilayer])
        BL     =  (  (BB + XX*BBA) / (1 + XX) )
    

        if ilayer <= itop_rad-1:
        
            # Why transmittance to layer top, temperature from base
            Bctsc0 = transL_lores * (plancknu(wnmres0,tzl[ilayer])).T
    	
    	
            # ... The layer radiance that reaches the surface
            rad0 = -BL * (transL-transLm1)
            # ... Reduce resolution
            wnmres,radL_lores = reduceRes(vLp, rad0, dnu, N, n)
            radL_loresp = np.interp(wnmf,wnmres,np.real(radL_lores)) + \
                			np.interp(wnmf,wnmres,np.imag(radL_lores))*1j
            radL_lores = radL_loresp.T
    	
    		 # ... Set the radiance-below-cloud and black body cloud emission
    		 #     for this layer (i.e. the layer with top ibnd2)
            Bctscp = (np.interp(wnmf,wnmres0,np.real(Bctsc0))).T + \
            		 (np.interp(wnmf,wnmres0,np.imag(Bctsc0))).T*1j 
            Bctsc[:,ilayer+1] = Bctscp
            tsc[:,ilayer+1]   = transL_lores  # trans to layer base
            rads[:,ilayer+1]  = radL_lores + rads[:,ilayer]   
            odt[:,ilayer+1]   = odt_lores
    
    
        if ilayer <= itop_od:
            # Optical depths
            odlayer[:,ilayer] = odtot_lores - odtot_below 
    	
    		 # Do not allow od<=0
            if len(zs)==1 and np.isnan(zs[0]):
                odlayer[odlayer[:,ilayer]<minod_below20km,ilayer]=minod_below20km  
            elif zs[ilayer]<=20:
                odlayer[odlayer[:,ilayer]<minod_below20km,ilayer]=minod_below20km  
            elif zs[ilayer]>20:
                odlayer[odlayer[:,ilayer]<minod_above20km,ilayer]=minod_above20km  

    	
        # Set values for next loop through
        wnm  = vLp
        transLm1  = transL
        odLm1 = odL
        odtot_below = odtot_lores

	
    # We should check that the imaginary parts are not too big here ...
    radsOut    = 1e3 * np.real(rads)
    BctscOut   = 1e3 * np.real(Bctsc)
    tscOut     = np.real(tsc)
    odlayerOut = np.real(odlayer)
    odtOut     = np.real(odt)
	
	
    return(wnmf, radsOut, BctscOut, tscOut, odlayerOut, odtOut)
	

