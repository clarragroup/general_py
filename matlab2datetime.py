#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 14:54:02 2019

matlab2datetime.py

Copyright 2019 by Penny M. Rowe and NorthWest Research Associates.
All rights reserved.

Purpose:
    Get python datetime object from matlab datenumber
"""

import datetime as dt

def matlab2datetime(matlab_datenum):

    def matlab2datetimeb(matlab_datenum):
      day = dt.datetime.fromordinal(int(matlab_datenum))
      dayfrac = dt.timedelta(days=matlab_datenum%1) - dt.timedelta(days = 366)
      return day + dayfrac

    if matlab_datenum.ndim==0:
      pydate =  matlab2datetimeb(matlab_datenum)
    else:
      pydate = [matlab2datetimeb(tval) for tval in matlab_datenum]
  
    return pydate
