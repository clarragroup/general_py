import numpy as np

def brightnessT(nu_icm, rad):
    # function f=brightnessT(nu_icm,rad);
    #
    # purpose: convert aeri radiances to brightness temperatures
    #
    # This code is based on plancnu_icm.m
    # and Dave Tobin's code rad2bt.m
    # created by Penny Rowe
    # April 7, 2001
    #
    # update Sept 2009: constants updated
    #         5-31-12:  new constants, CODATA 2010 -ccox
    #
    # Inputs
    #   nu_icm: wavenumber, vector
    #   rad: radiance, (RU), vector
    
    
    # Constants
    h    = 6.62606957e-34				# J s;  CODATA 2010
    c    = 2.99792458e8				# m/s;  NIST
    k    = 1.3806488e-23				# J K-1; CODATA 2010
    cbar = 100*c;			  		# cm/s
    
    
    rad = rad*1e-3		# needs to be W m-2 str cm-1-1
    
    top = 2 * h * cbar**3 * nu_icm**3
    a = h * cbar * nu_icm/k
    
    arg = ( (cbar*top) / (c**2 * rad) + 1 )
    
    # This part is taken from Dave Tobin's code
    if np.any(arg<=0):
        ineg = np.where(arg <= 0)[0]
        arg[ineg] = 1e-3
        f = a / np.log(  arg   )
        f[ineg] = 0
    else:        
        f = a / np.log(  arg   ) 
        ineg = []
    
    
    # now set f to zero for wavenumbers less than or equal to 0
    if min(nu_icm) <= 0:
        indzero = np.where(nu_icm<=0)[0]
        f[indzero] = 0
    
    #[B]= cm*s-1*J*s*cm3*s-3*cm-3*m-2*s2
    #[B]= W m-2 cm
    	
    # This part is taken from Dave Tobin's code
    #if np.any(rad<=0):
    #    inegs = np.where(rad <= 0)[0]
    #    f[inegs] = 0
        
    #if np.any(np.real(rad)==False):
    #    # add code here
    
    #if length(ind) ~= 0
    #   disp('WARNING: negative and/or imaginary inputs to RAD2BT')
    #end
    
    
    return  f, ineg
    
    # Old constant values
    # h = 6.6260687652e-34;     		# J s
    # c = 299792458;                    # m s-1
    # cbar=100*c;
    # k = 1.380650324e-23;      			# J K-1
    #
    # constants
    # Mohr and Taylor, Physics Today, Buyer's Guide, Supplement
    # to the August 2000 Issue of Physics Today, pgs. BG6-BG13:
    #
    #      constant                   value                      units    
    #Relative standard uncertainty
    #  speed of light in a vacuum   299792458                    m s-1    
    #(exact)
    #
    #  Planck constant (h)          6.62606876(52) x 10(-34)     J s      
    #7.8 x 10(-8)
    #
    #  Boltzmann constant (k)       1.3806503(24) x 10(-23)      J K-1    
    #1.7 x 10(-6) 
    #
    
