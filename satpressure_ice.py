def satpressure_ice(T_c):

    # By Penny M. Rowe
    # Converted from matlab code 2013/5/12

    # Notes from satpressure_ice.m
    #
    # function Psat_ice = satpressure(T_c)
    #
    # units:  Input temp in celcius, will convert to Kelvin, pressure in Pa
    #
    # Reference:
    #
    # Marti, James and Konrad Mauersberger, A Survey of New Measurements of Ice Vapor Pressure 
    # at Temperatures Between 170 and 250K, Geophys. Res. Let., vol. 20, No. 5, pp. 363-366, 1993.
    #
    # Notes:  The frost point is the point to which you must cool air at constant pressure in
    # 	order for frost to form.  Thus at the frost point, Pambient = Psat over ice, and
    #	Psat(T) as shown below.
    #
    #
    # Penny Rowe
    #
    # Dec. 30, 1999

    # constants
    A = -2663.5     # +-.8, Kelvin
    B = 12.537      # +- 0.11

    T_K      = T_c+273.15     # convert celcius to kelvin
    logp     = A/T_K + B
    Psat_ice = 10**(logp)    # Pa
    
    return Psat_ice

    # ERROR ANALYSIS
    #
    # dP/dT = A*P/(T^2);
    # 
    # sigmaP = (A*P/(T^2)) * sigmaT
    #




