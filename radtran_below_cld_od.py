
import numpy as np
from plancknu import plancknu


def radtran_below_cld_od(wnm, od, tzl, tave, bltype, direction, radsurf):
    """
		#
		#
		# by Penny Rowe
		# May 30, 2001
		# Updated Aug. 7, 2002
		# Updated Feb. 2012: added odDir
		# Updated May 23, 2013: 
		#   Improved comments
		#   Changed rdODsingle to rdODprec, added "prec" (precision) to inputs
		#   Changed name from radtranC to radtran_lorestrans
		#   Added possibilities "lowClough13" and "lowCloughPade" to bltype
		#   No longer cd to directory where ods are
		# Updated Jan. 11, 2013
		#   Can do up or downwnwelling radiance, based on direction
		# Updated Oct. 4, 2016
		#   changed outputs
		#
		#
		# Purpose: 
		#   Get low- or perfect-resolution radiance (and corresponding wavenumber)
		#   by performing radiative transfer in the same way as LBLRTM,
		#   except that Eqn (13) of Clough et al. 1992 is used instead of
		#   getting the Planck function empirically using a Pade coefficient
		#   as in Eqn (15), *or* using the Pade function, or using the method
		#   of Wiscombe 1976.  Also, can do low or perfect resolution.  These
		#   Choice depend on input variable bltype.
		#
		#   Low-resolution is acheived by reducing the resolution of the
		#   transmittances, not the radiances
		#
		# .. Inputs to function call:
		#    wnm  = wavenumber, numpy 1-d array
		#    od   = layer optical depths, numpy array [itop x len(wnm)]
		#    itop = number of layers for which there are ods
		#    tzl  = temperature at layer bottom, from surface up
		#    tave = "ave" temperature, as defined by LBLRTM
		#    dnu  = desired resolution
		#    bltype: lowClough13, lowClough/lowCloughPade, hiClough13, lowWiscombe
		#
		#    izeroout  removed!
		#
		# .. Outputs:
		#    rad: radiance, numpy 1-d array
		# # # # # # # # # # # #  # # # # #  # # # #  # # # #  # # # # # # #
		"""

    itop = len(tzl)-1
    #if size(od) ~= [itop length(wnm)]
    if od.shape != (itop, len(wnm)):
      raise ValueError('Input dimensions of optical depth are wrong.')
    # end
    
    if (len(tave)==1 and tave[0]==0) or sum(tave)<1e-6:
      tave = (tzl[:itop]+tzl[1:itop+1])/2
      #print('tave=0, so creating tave from temps.')
    # end
    
    # Choose direction
    #if strcmpi(direction,'down')
    if direction == 'down': # if direction is down
      # Downwelling radiance.  Start from surface and go up
      upflag=0
      #ilayers = 1:itop
      ilayers = list(range(itop))
    #elseif strcmpi(direction,'up')
    elif direction == 'up':
      # Upwelling radiance.  Start from TOA and go down
      #ilayers = itop:-1:1 # what does this do?
      ilayers = list(range(itop, 0, -1))
      upflag = 1 
    # end
    
    # Preallocate
    rad    = np.zeros([len(wnm)])                     #Rabovecloud = 0; Rbelowcloud = 0
    rads   = np.zeros([len(wnm),len(ilayers)+1])
    tsc    = np.ones( [len(wnm),len(ilayers)+1])
    transL = np.ones( [len(wnm)])
    # get transmittance, rad., from the bottom layer up
    #for ilayer=ilayers
    for ilayer in ilayers:
      
      if upflag:
        iaslayer = ilayer
      else:
        iaslayer = ilayer+1
        
      transLm1 =  transL
      #tralyr =  exp(-od(ilayer,:))
      tralyr =  np.exp(-od[ilayer,:])
      
      
      #transL = transLm1 .* tralyr        # surf to layer trans
      transL = transLm1 * tralyr          # surf to layer trans
      tsc[:,iaslayer] = transL
     
      
      if bltype == 'lowClough13':
          # Uses low resolution layer optical depth
          # See Clough et al. 1992 Eqn. (13) # PMR May 23, 2013
          Bb   =  plancknu(wnm,tave[ilayer]) 
          if upflag:
            Bu   =  plancknu(wnm,tzl[ilayer+1])     # layer top
          else:
            # Note that tzl is from the bottom up here, because 
            # we are going from the TOA down, and
            # tzl(itop) or the temp at TOA, is used first
            Bu   =  plancknu(wnm,tzl[ilayer]) 
          # end
          # Eqn (13) gives NaN when odlyr=0, so reset to 1e-16
          # same for tylrLo=1, so reset to 1-1e-16
          #tlyrLo(odlyr(:,ilayer)<=0) = exp(-1e-6) 
          #odlyr(odlyr(:,ilayer)<=0,ilayer)=1e-6
          BL   =  Bu + 2*(Bb-Bu)*(1/od[ilayer,:] - tralyr/(1-tralyr))
          rad0 = -BL * (transL-transLm1)               # Eqn (14)
          if np.any(np.isnan(rad0)): 
              raise ValueError('found an nan')  # end      
      elif bltype ==  'lowClough' or  bltype == 'lowCloughPade':
          # Uses low resolution layer optical depth
          # and Pade coefficient from Clough et al. 1992 Eqn. (14)
          # PMR May 23, 2013
          Bb   =  plancknu(wnm,tave[ilayer]) 
          if upflag:
            Bu   =  plancknu(wnm,tzl[ilayer+1])     # layer top
          else:
            Bu   =  plancknu(wnm,tzl[ilayer]) 
          # end
          atau = 0.278*od[ilayer,:]                  # Pade coeff. x tau
          BL   = (Bb + atau*Bu) * (1+atau)**-1      # Eqn (15)
          rad0 = -BL * (transL-transLm1)            # Eqn (14)
          if np.any(np.isnan(rad0)): 
              raise ValueError('found an nan')  # end      
      elif bltype == 'lowWiscombe':
          # See Wiscomb 1976, uses low resolution layer optical depth
          if upflag:
            Bo   =  plancknu(wnm,tzl[ilayer+1])[0,:]
            Bn   =  plancknu(wnm,tzl[ilayer])[0,:] 
          else:
            Bo   =  plancknu(wnm,tzl[ilayer]) #[0,:]
            Bn   =  plancknu(wnm,tzl[ilayer+1]) #[0,:]
          # end
          ## Divide by odlyr below here, so set zeros to small value
          #tL_Lo(odlyr(:,ilayer)==0) = tLm1Lo(odlyr(:,ilayer)==0) * exp(-1e-6) 
          #odlyr(odlyr(:,ilayer)==0,ilayer)=1e-6
          rad0 =  ( Bo*(transLm1-transL) + 
            (Bn-Bo)*( -transL + (transLm1-transL) / od[ilayer,:] ) )  
          #if find(isnan(rad0)) error('found an nan')  # end 
      else:
          raise ValueError('Bad value for  bltype')
      # end
      
      rads[:,iaslayer] = 1e3*np.real(rad0)
      rad += rad0
      
      """
      if upflag:
        if ilayer==cloudlayer+1:
          Rabovecloud = rad
        elif ilayer<=cloudlayer:
          Rbelowcloud = Rbelowcloud+rad0
        # end
      else:
        if ilayer==cloudlayer-1:
          Rbelowcloud = rad
        elif ilayer>=cloudlayer:
          Rabovecloud = Rabovecloud+rad0
        # end
      # end
      """
    
    # end 

    if upflag:
      # Add on surface emission
      #Rs          = (radsurf.*transL)
      Rs          = (radsurf*transL)
      rad         = rad + Rs
      rads[:,ilayer+1] = Rs
      Rbelowcloud = Rbelowcloud + Rs 
    # end
    
    # QC
    if sum(np.imag(rad))<1e-10:
      rad=np.real(rad) 
    else:
      raise ValueError('Non-trivial imaginary part')
    # end
    
    #if sum(np.imag(Rabovecloud))<1e-10 :
    #  Rabovecloud=np.real(Rabovecloud)
    #else:
    #  raise ValueError('Non-trivial imaginary part')
    # end
    
    #if sum(np.imag(Rbelowcloud))<1e-10 :
    #  Rbelowcloud=np.real(Rbelowcloud)
    #else:
    #  raise ValueError('Non-trivial imaginary part')
    # end
    
    ## rad         = 1e3*rad'
    ## Rbelowcloud = 1e3*Rbelowcloud'
    ## Rabovecloud = 1e3*Rabovecloud'
    
    rad         = 1e3*rad
    #Rbelowcloud = 1e3*Rbelowcloud
    #Rabovecloud = 1e3*Rabovecloud
    
    return rad,rads,tsc
